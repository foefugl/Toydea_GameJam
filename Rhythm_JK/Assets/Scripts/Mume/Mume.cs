﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class DressHP
{
    private float[] dress = new float[3];
    /* 0 is upper garment
    * 1 is middle garment
    * 2 is lower garment
    */
    public int getDressLength()
    {
        return dress.Length;
    }
    public float getHP(int _whitchDress)
    {
        return dress[_whitchDress];
    }
    public void setHP(int _whitchDress, float _hp)
    {
        dress[_whitchDress] = _hp;
    }
    public void reduceHP(int _whitchDress, float value)
    {
        if (dress[_whitchDress] - value > 0f)
            dress[_whitchDress] -= value;
        else
            dress[_whitchDress] = 0f;
    }
    public float getTotalDressHP()
    {
        float sum = 0f;
        for (int i = 0; i < dress.Length; i++)
            sum += dress[i];
        return sum;
    }
    public void init()
    {
        for (int i = 0; i < dress.Length; i++)
        {
            setHP(i, 100f);
        }
    }
    public DressHP()
    {
        init();
    }
}
public class Mume : MonoBehaviour
{
    // Start is called before the first frame update
    DressHP dressHP = new DressHP();

    public double wantDestroyParticularDress(int _whitchDress)
    {
        // input the index of dress, this function will return the probilibity of user if wantDestroyParticularDress
        // the return probilibity is in [0, 1]
        // 1 is abslutely want to destroy the dress
        // on the other hand, 0 is not at all.
        // Usage: 
        //      upperDressProbility = wantDestroyParticularDress(0)
        // upperDressProbility is the probilibity that user want to destroy.

        return Math.Cos(dressHP.getHP(_whitchDress) * Math.PI * 3 / dressHP.getTotalDressHP() / 2);
    }
    private bool canDestroyDress(int _whitchDress)
    {
        return dressHP.getHP(_whitchDress) <= 0;
    }
    public void upperDressDestroy()
    {
        if (canDestroyDress(0))
        {

        }
    }
    public void middleDressDestroy()
    {
        if (canDestroyDress(1))
        {

        }
    }
    public void lowerDressDestroy()
    {
        if (canDestroyDress(2))
        {

        }
    }
    public void reduceDressHP(int _whitchDress, float value)
    {
        dressHP.reduceHP(_whitchDress, value);
    }
    void Start()
    {
        dressHP.init();
        //dressHP.reduceDressHP(0,10);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(wantDestroyParticularDress(0));
        //reduceDressHP(0, 2);
    }
}