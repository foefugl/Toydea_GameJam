﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Ball : MonoBehaviour
{
    // Start is called before the first frame update
    osuBeat pooling;
    private int t;
    public void init(int _time)
    {
        t = _time;
    }
    public bool isVisible = false;

    public osuBeat Pooling { get => pooling; set => pooling = value; }  // i don't know what is this

    public void setPos(int _time)
    {
        if (isVisible)
        {

            // should get time per frame
            // (end place  - appear place ) is the distence
            // this distence divide by the time per frame
            // every frame move to the correct place
            // leaf?
        }
    }
    public void setVisible()
    {
        // if the pos of x, y, z is out of the range
        //Transform.position;
    }
    public void Destroy(){
        this.isVisible = false;
        Pooling.Recycle(this);
    }
    public int onHitting(ref int currentIndex)
    {
        if(!isVisible)
            return 0;

        int diffTime = Math.Abs((int)(Time.timeSinceLevelLoad * 1000f) - t);
        // if Destroy() current++
        // return Score
        if (diffTime > 200)
        {
            return -20;
        }
        else if (diffTime > 100)
        {
            // not bad

            return 10;
        }
        else if (diffTime > 50)
        {
            // good

            return 40;
        }
        else
        {
            // perfect

            return 100;
        }
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int currentTimeByMS = Convert.ToInt32(Time.timeSinceLevelLoad * 1000f);
        // if (currentTimeByMS - this.t <= (distance divide bymoving speed) )
        //      isVisible = true
        setPos(currentTimeByMS);
    }
}
