﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using DG.Tweening;

public class osuBeat : MonoBehaviour
{
    static string path = "Assets/Resources/beatMap.txt";
    string[] beatMapData = File.ReadAllLines(path);
    private const int BALLNUM = 30;
    private int currentIndex = 0;
    // Start is called before the first frame update
    public GameObject[] ballPool = new GameObject[BALLNUM];
    public GameObject ball;
    private int bonusMeter = 0; // add bonus meter to let user play for "Invincible mode" for some meters
    Queue<Ball> unusedBall = new Queue<Ball>();

    public Ball generateBall(){
        if(currentIndex == beatMapData.Length -1)
            return null;
        GameObject nextBall = Instantiate(ball, new Vector3(0, 0, 0), Quaternion.identity);
        nextBall.GetComponent<Ball>().init(Int32.Parse(beatMapData[currentIndex]));
        currentIndex++;
        return nextBall.GetComponent<Ball>();
    }
    public void playerController()
    {
        var verticalInput = Input.GetAxisRaw("Vertical");
        if (verticalInput != 0)
        {
            // to judge if the hit point is touched the ball
            // and let the ball Destroy
            // how to know whitch ball hit me?
            // bonusMeter += ballPool[currentIndex + 1].GetComponent<Ball>().onHitting(ref currentIndex);
            // do some effect
        }
    }

    void Start()
    {
        //Debug.Log(beatMapData[i]);
        bonusMeter = 0;
        unusedBall.Clear();
        GameObject firstBall = Instantiate(ball, new Vector3(0, 0, 0), Quaternion.identity);
        firstBall.GetComponent<Ball>().init(Convert.ToInt32(beatMapData[0]));
        unusedBall.Enqueue(firstBall.GetComponent<Ball>());

    }

    // Update is called once per frame
    void Update()
    {
        playerController();
        //nextBall += unusedBall.Dequeue();
        //nextBall.init();
        //if(ballPool[currentIndex+1] > (int)(Time.timeSinceLevelLoad * 1000f) > 200)
    }


    public void Recycle(Ball collections){
        //enqueue
        unusedBall.Enqueue(collections);
    }

}
