﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering.PostProcessing;

public class FeverManager : MonoBehaviour
{
    public PostProcessVolume processVolume;
    private ColorGrading colorgrade;
    public static FeverManager Instance;
    public FeverCircle feverCircle;
    public GameObject FeverSkyObj;
    public EventObjHome objHome;
    public BPeerM bPeerM;
    public PlayerController controller;
    public Transform playerTrans;
    public int feverCountLimit = 10;
    private int levelCount = 0;
    public bool feverHappen = false;
    private float targetValue = 180;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
    }
    private void Start()
    {
        processVolume.profile.TryGetSettings(out colorgrade);

        ComboManager.Instance.Regist(ComBoEvent.ValueAdd, FeverJudge);
        FeverSkyObj.SetActive(false);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            FeverJudge();
        }
        
        if(feverHappen)
        {
            if(colorgrade.hueShift.value  >= 178)
            {
                targetValue = -180;
            }
            else if(colorgrade.hueShift.value  <= -178)
            {
                targetValue = 180;
            }
            colorgrade.hueShift.value  = Mathf.Lerp(colorgrade.hueShift.value, targetValue, Time.deltaTime);
        }
        else
        {
            colorgrade.hueShift.value = Mathf.Lerp(colorgrade.hueShift.value, 0, Time.deltaTime);
        }
    }

    private void FeverJudge()
    {
        if(feverHappen)
            return;
            
        if(ComboManager.Instance.ComboCount % feverCountLimit == 0 && !FeverManager.Instance.feverHappen)
        {
            feverHappen = true;
            StartCoroutine(FeverEffect());
        }
    }
    IEnumerator FeverEffect()
    {
        float originBPM = bPeerM._bpm;
        Camera.main.transform.DOShakePosition(0.5f, 0.25f, 50, 90, false, true).SetLoops(7).SetDelay(0.5f);
        FeverSkyObj.SetActive(true);
        StopObjMovingFunction();
        playerTrans.GetComponent<Collider>().isTrigger = true;
        playerTrans.DOScale(Vector3.one * 1.5f, 1).SetEase(Ease.OutExpo);

        yield return new WaitForSeconds(1);
        bPeerM._bpm *= 1.5f;
        var allObjs = FindObjectsOfType<ItemBehaviour>();
        foreach(var obj in allObjs)
        {
            obj.speed = 5;
        }
        StartObjMovingFunction();
        objHome.createStatus = EventObjHome.CreateStatus.RewardTime;
        playerTrans.DOShakeScale(0.25f, 1, 10, 90, true).SetLoops(20).SetEase(Ease.OutExpo);
        objHome.speed = 5;
        yield return new WaitForSeconds(5);
        playerTrans.DOScale(Vector3.one, 1).SetEase(Ease.OutExpo);
        FeverSkyObj.SetActive(false);
        playerTrans.GetComponent<Collider>().isTrigger = false;

        bPeerM._bpm = 40;
        objHome.speed = 1;
        var allObjsAfter = FindObjectsOfType<ItemBehaviour>();
        controller.enabled = true;
        foreach(var obj in allObjs)
        {
            obj.speed = 1;
        }
        yield return new WaitForSeconds(1);
        objHome.createStatus = EventObjHome.CreateStatus.BompTime;
        feverHappen = false;
    }
    private void StopObjMovingFunction()
    {
        bPeerM.enabled = false;
        controller.enabled = false;

        var objs = GameObject.FindObjectsOfType<ItemBehaviour>();
        foreach (var item in objs)
        {
            item.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    private void StartObjMovingFunction()
    {
        bPeerM.enabled = true;

        var objs = GameObject.FindObjectsOfType<ItemBehaviour>();
        foreach (var item in objs)
        {
            item.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
