﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public Transform[] roadPos;
    public float duration;
    public Ease easeType;
    private int currentRoadIndex = 0;

    // Update is called once per frame
    void Update()
    {
        var verticalInput = Input.GetAxisRaw("Vertical");
        if (verticalInput != 0)
        {
            if (verticalInput < 0 && currentRoadIndex == 0)
                currentRoadIndex++; //go down
            else if (verticalInput > 0 && currentRoadIndex == 1)
                currentRoadIndex--; //go up
            transform.DOMove(roadPos[currentRoadIndex].position, duration).SetEase(easeType);
        }
    }
}
