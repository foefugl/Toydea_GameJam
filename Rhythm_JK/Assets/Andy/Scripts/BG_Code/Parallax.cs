﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Transform brick;
    public int createCount = 1;
    public float fakeLength;
    private float length, startpos, distanceFromCamera;
    public float parallaxEffect;
    private float currentSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0 ; i<createCount ; i++)
        {
            var brickBoy = Instantiate(brick, transform.position + Vector3.right * fakeLength * (i+2), transform.rotation, this.transform);
        }
        startpos = transform.position.x;
        // length = GetComponent<SpriteRenderer>().bounds.size.x;
        length = fakeLength;
    }

    private void Update()
    {
        // float temp = (Time.time * BPeerM._BPeerMInastance._bpm/60 * (1 - parallaxEffect));
        float dist = (Time.time * BPeerM._BPeerMInastance._bpm/60 * parallaxEffect);
        currentSpeed = Mathf.Lerp(currentSpeed, dist, Time.deltaTime);
        transform.position = new Vector3(startpos + currentSpeed, transform.position.y, transform.position.z);
        // if(temp > startpos + length)startpos += length;
        // else if(temp < startpos - length) startpos -= length;
    }
}
