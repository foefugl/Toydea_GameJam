﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundsOnBeat : MonoBehaviour
{
    public TempoSoundManager _tempoSoundManager;
    public AudioClip _tap, _tick;
    public AudioClip[] _strum;
    int _randomStrum;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(BPeerM._beatFull)
        {
            // _tempoSoundManager.PlaySound(_tap, 1);
            if(BPeerM._beatCountFull % 2 == 0)
            {
                _randomStrum = Random.Range(0, _strum.Length);
            }
        }
        if(BPeerM._beatD8 && BPeerM._beatCountD8 % 2 == 0)
        {
            // _tempoSoundManager.PlaySound(_tick, 0.1f);
        }
        if(BPeerM._beatD8 && (BPeerM._beatCountD8 % 8 == 2 || BPeerM._beatCountD8 % 8 == 4))
        {
            // _tempoSoundManager.PlaySound(_strum[_randomStrum], 1);
        }
    }
}
