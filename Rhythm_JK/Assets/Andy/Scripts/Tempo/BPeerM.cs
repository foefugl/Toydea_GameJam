﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class BPeerM : MonoBehaviour
{
    public EventObjHome eventObjHome;
    #region ObstacleTable
    string obs1 = "wweseeweseesewes";
    string obs2 = "eeesewesseweseww";
    string obs3 = "sewwesseeewesewe";
    string obs4 = "eweeseeweseesewe";
    #endregion
    #region RewardTable
    string reward1 = "wswswwwwssswswsw";
    string reward2 = "swswssssswwwswsws";
    string reward3 = "wwwsswwwswswsswsw";
    string reward4 = "wsssssswwwswsswws";
    #endregion
    private List<string> obstacleTables, rewardTables;
    private string currentCompose;
    private int currentNoteIndexInTable = 0;
    private bool tableSetting = false, tablleFinish = false;
    public AudioSource BGM_Source;
    public static BPeerM _BPeerMInastance;
    public float _bpm;
    private float _beatInterval, _beatTimer, _beatintervalD8, _beatTimerD8;
    public static bool _beatFull, _beatD8;
    public static int _beatCountFull, _beatCountD8;

    private int bulletCycleCount = 1;
    public int bulletCycleLimitObstacle = 2;
    private int currentCycleLimit;

    private void Awake()
    {
        obstacleTables = new List<string>();
        obstacleTables.Add(obs1);
        obstacleTables.Add(obs2);
        obstacleTables.Add(obs3);
        obstacleTables.Add(obs4);

        rewardTables = new List<string>();
        rewardTables.Add(reward1);
        rewardTables.Add(reward2);
        rewardTables.Add(reward3);
        rewardTables.Add(reward4);
        
        if(_BPeerMInastance != null && _BPeerMInastance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _BPeerMInastance = this;
        }
    }
    private void Update()
    {
        BeatDetection();
    }
    private void BeatDetection()
    {
        //full beat count
        _beatFull = false;
        _beatInterval = 60 / _bpm;
        _beatTimer += Time.deltaTime;
        if(_beatTimer >= _beatInterval)
        {
            bulletCycleCount ++;
            PlayBGM();
            CreateTableRandomByState();

            _beatTimer -= _beatInterval;
            _beatFull = true;
            _beatCountFull++;
        }
        // divided beat count
        _beatD8 = false;
        _beatintervalD8 = _beatInterval / 8;
        _beatTimerD8 += Time.deltaTime;
        if(_beatTimerD8 >= _beatintervalD8)
        {
            if(tableSetting)
            {
                if(currentNoteIndexInTable < currentCompose.Length -1)
                {
                    eventObjHome.CreateNote(currentCompose[currentNoteIndexInTable].ToString());
                }
                else if( eventObjHome.createStatus == EventObjHome.CreateStatus.BompTime && bulletCycleCount % currentCycleLimit == 1)
                {
                    tableSetting = false;
                }
                else if(eventObjHome.createStatus == EventObjHome.CreateStatus.RewardTime)
                {
                    tableSetting = false;
                }
            }
            currentNoteIndexInTable++;

            _beatTimerD8 -= _beatintervalD8;
            _beatD8 = true;
            _beatCountD8 ++;
            // Debug.Log("D8");
        }
    }
    private void PlayBGM()
    {
        if(!BGM_Source.isPlaying)
        {
            BGM_Source.Play();
        }
    }
    private void CreateTableRandomByState()
    {
        if(tableSetting == false)
        {
            int composeArrayIndex = -1;
            if(eventObjHome.createStatus == EventObjHome.CreateStatus.BompTime)
            {
                currentCycleLimit = bulletCycleLimitObstacle;
                composeArrayIndex = Random.Range(0, obstacleTables.Count);
                currentCompose = obstacleTables[composeArrayIndex];
            }
            else
            {
                composeArrayIndex = Random.Range(0, rewardTables.Count);
                currentCompose = rewardTables[composeArrayIndex];
            }
            currentNoteIndexInTable = 0;
            tableSetting = true;
        }
    }
}
