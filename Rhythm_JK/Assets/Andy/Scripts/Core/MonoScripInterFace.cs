﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface MonoScripInterFace 
{
    int frameCountUpdate{get; set; }
    void Initial();
    void FrameUpdate();

}
