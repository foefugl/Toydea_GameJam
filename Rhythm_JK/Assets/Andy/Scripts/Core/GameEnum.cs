﻿public enum GameEvent
{
    upperCollect = 0,
    lowerCollect = 1,
    obstaclesCollect = 2,
    upperDamage = 3,
    lowerDamage = 4,
    obstacleDamage = 5
}