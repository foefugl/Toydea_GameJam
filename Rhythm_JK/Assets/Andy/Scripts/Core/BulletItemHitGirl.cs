﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BulletItemHitGirl : MonoBehaviour
{
    public GameEvent eventType;
    public float colorFadeSpd;
    public SpriteRenderer spriteRend;
    private Color oriColor;
    private Tweener hitGirlTwn;
    private Vector3 defaultPos;
    private bool isDamaged = false;
    private void Start()
    {
        oriColor = spriteRend.color;
        defaultPos = spriteRend.transform.position;
    }
    private void Update()
    {
        spriteRend.color = Color.Lerp(spriteRend.color, oriColor, Time.deltaTime * colorFadeSpd);
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.gameObject.name);
        var missile = other.transform.GetComponent<Missile>();

        if(missile == null)
            return;

        if(isDamaged)
            return;

        isDamaged = true;
        GirlPosHittedEff();
    }

    IEnumerator WaitOneSecondToDamage()
    {
        yield return new WaitForSeconds(0.75f);
        isDamaged = false;
    }

    public void GirlPosHittedEff()
    {
        spriteRend.gameObject.transform.DOShakePosition(0.15f, 0.15f, 50, 90, false, true).OnComplete(() => PosInitial());
        spriteRend.color = Color.red;
    }
    private void PosInitial()
    {
        spriteRend.transform.position = defaultPos;
    }
}
