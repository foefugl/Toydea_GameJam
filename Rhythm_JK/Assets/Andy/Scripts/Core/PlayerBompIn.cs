﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody))]
public class PlayerBompIn : MonoBehaviour
{
    public Renderer charactorRend;
    private GameObject playerObj;
    private Vector3 defaultScale, defaultPos;
    private Tweener currentTweener;
    private Sequence jumpSeq;

    private void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
        defaultScale = transform.localScale;
        defaultPos = transform.position;

        jumpSeq = playerObj.transform.DOLocalJump(playerObj.transform.position, 1, 1, 0.4f, false).OnComplete(() => PosInitial());
        jumpSeq.Pause();

        BompInCalculateSystem.Instance.Regist(GameEvent.obstaclesCollect, BompObstacleEffAction);
        BompInCalculateSystem.Instance.Regist(GameEvent.lowerCollect, BompUpperEffAction);
        BompInCalculateSystem.Instance.Regist(GameEvent.upperCollect, BompUpperEffAction);
    }
    private void Update()
    {
        DamageColorRevover();
    }

    private void BompUpperEffAction()
    {
        if(jumpSeq.IsPlaying())
        return;

        jumpSeq.Play();
    }
    private void BompLowerEffAction()
    {
        if(jumpSeq.IsPlaying())
        return;

        jumpSeq.Play();
    }

    private void BompObstacleEffAction()
    {
        currentTweener = playerObj.transform.DOShakeScale(0.3f, 1, 10, 90, true).OnComplete(() => ScaleInitial());
        DamageColorEffect();
    }
    private void OnCollisionEnter(Collision other)
    {
        var itemBehaviour = other.gameObject.GetComponent<ItemBehaviour>();

        if(itemBehaviour == null)
            return;
        
       ItemBompInToFire(itemBehaviour);
    }
    public void ItemBompInToFire(ItemBehaviour itemBehaviour)
    {
        var missileLaunch = transform.GetComponentInChildren<MissileLauncher>();
        missileLaunch.currentHitItem = itemBehaviour.type;
        
        BompInCalculateSystem.Instance.EventTrigger(itemBehaviour.type);
    }

    private void DamageColorEffect()
    {
        // charactorRend.material.color = Color.red;
    }
    private void DamageColorRevover()
    {
        // charactorRend.material.color = Color.Lerp(charactorRend.material.color, Color.white, Time.deltaTime);
    }
    private void ScaleInitial()
    {
        transform.localScale = defaultScale;
    }
    private void PosInitial()
    {
        transform.localScale = defaultScale;
    }
}
