﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TimerCount
{
    private float limit;
    private float count;
    public TimerCount(float _limit)
    {
        limit = _limit;
    }
    public float GetLimit
    {
        get
        {
            return limit;
        }
    }
    public void TimerUpdate()
    {
        count += Time.deltaTime;
    }
    public bool IsFinish()
    {
        return (count >= limit) ? true : false ;
    }
    public float CurrentCount()
    {
        return count;
    }
}
