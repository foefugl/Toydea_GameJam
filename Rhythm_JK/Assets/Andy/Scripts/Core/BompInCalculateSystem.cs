﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BompInCalculateSystem : MonoBehaviour
{
    public static BompInCalculateSystem Instance;
    List<Action> LowerCollectActions;
    List<Action> UpperCollectActions;
    List<Action> ObstacleCollectActions;
    List<Action> LowerDamageActions;
    List<Action> UpperDamageActions;
    List<Action> ObstacleDamageActions;
    Dictionary<GameEvent, List<Action>> EventDic;

    // Start is called before the first frame update
    void Awake()
    {
        EventDic = new Dictionary<GameEvent, List<Action>>();
        LowerCollectActions = new List<Action>();
        UpperCollectActions = new List<Action>();
        ObstacleCollectActions = new List<Action>();

        UpperDamageActions = new List<Action>();
        LowerDamageActions = new List<Action>();
        ObstacleDamageActions = new List<Action>();

        EventDic.Add(GameEvent.lowerCollect, LowerCollectActions);
        EventDic.Add(GameEvent.upperCollect, UpperCollectActions);
        EventDic.Add(GameEvent.obstaclesCollect, ObstacleCollectActions);

        EventDic.Add(GameEvent.lowerDamage, LowerDamageActions);
        EventDic.Add(GameEvent.upperDamage, UpperDamageActions);
        EventDic.Add(GameEvent.obstacleDamage, ObstacleDamageActions);

        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
    }
    public void Regist(GameEvent eventType, Action action)
    {
        var actionList = EventDic[eventType];
        actionList.Add(action);
    }
    public void EventTrigger(GameEvent eventType)
    {
        var tempToTri = EventDic[eventType];

        if(tempToTri == null){
            Debug.Log("ItemType " + eventType + " no action regist!");
            return;
        }
            

        foreach(var action in tempToTri)
        {
            action();
        }
    }
}
