﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeverCircle : MonoBehaviour
{
    public Transform player;
    private  SphereCollider sphereCollider;
    private PlayerBompIn bompInScript;
    private List<ItemBehaviour> triItem;
    private void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
        triItem = new List<ItemBehaviour>();
        bompInScript = player.GetComponent<PlayerBompIn>();
    }
    private void Update()
    {
        if(FeverManager.Instance.feverHappen)
        {
            sphereCollider.enabled = true;
        }
        else
        {
            sphereCollider.enabled = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var itemBehaviour = other.gameObject.GetComponent<ItemBehaviour>();
        if(itemBehaviour == null)
            return;

        if(itemBehaviour.type == GameEvent.obstaclesCollect)
            return;

        if(triItem.Contains(itemBehaviour))
            return;

        triItem.Add(itemBehaviour);
        itemBehaviour.target = player;
        StartCoroutine(AutoCollectByPlayer(0.35f, itemBehaviour));

    }
    IEnumerator AutoCollectByPlayer(float time, ItemBehaviour itemBerhaviour)
    {
        yield return new WaitForSeconds(time);
        bompInScript.ItemBompInToFire(itemBerhaviour);
        triItem.Remove(itemBerhaviour);
        if(itemBerhaviour != null )itemBerhaviour.DestroyEvent();
    }
}