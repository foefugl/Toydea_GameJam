﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public enum ComBoEvent
{
    ValueAdd,
    ValueTurnZero
}
public class ComboManager : MonoBehaviour
{
    public static ComboManager Instance;
    public int comboCountToTriggerFire = 20;
    public Text comboTxt;
    private int combo;
    public int ComboCount{ get{ return combo; } }
    private Vector3 comboTxtDefaultSize, comboTxtDefaultPos;
    private Dictionary<ComBoEvent, List<Action>> EventDic;
    private List<Action> ValueAddActions;
    private List<Action> ValueTurnZeroActions;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
        EventDic = new Dictionary<ComBoEvent, List<Action>>();
        ValueAddActions = new List<Action>();
        ValueTurnZeroActions = new List<Action>();

        EventDic.Add(ComBoEvent.ValueAdd, ValueAddActions);
        EventDic.Add(ComBoEvent.ValueTurnZero, ValueTurnZeroActions);
    }

    // Start is called before the first frame update
    void Start()
    {
        BompInCalculateSystem.Instance.Regist(GameEvent.lowerCollect, AddCombo);
        BompInCalculateSystem.Instance.Regist(GameEvent.upperCollect, AddCombo);
        BompInCalculateSystem.Instance.Regist(GameEvent.obstaclesCollect, TurnZero);
        comboTxtDefaultSize = comboTxt.transform.localScale;
        comboTxtDefaultPos = comboTxt.transform.position;
        TurnZero();
    }
    public void Regist(ComBoEvent eventType, Action action)
    {
        var listToAdd = EventDic[eventType];
        listToAdd.Add(action);
    }


    private void EventTrigger(ComBoEvent type)
    {
        var list = EventDic[type];
        foreach(var manber in list)
        {
            manber();
        }
    }
    private void AddCombo()
    {
        combo ++;
        ComboUpdate();
        AddCombeEff();
        EventTrigger(ComBoEvent.ValueAdd);
    }

    private void TurnZero()
    {
        combo = 0;
        TurnZeroEff();
        ComboUpdate();
        EventTrigger(ComBoEvent.ValueTurnZero);
    }
    private void ComboUpdate()
    {
        comboTxt.text = combo.ToString();
    }

    private void AddCombeEff()
    {
        // if(FeverManager.Instance.feverHappen)
        //     return;
        comboTxt.transform.DOPunchScale(Vector3.one * 2, 0.25F, 10, 1).OnComplete(() => TransformdateInitial());
    }
    private void TurnZeroEff()
    {
        // if(FeverCircle.Instance.gameObject.activeSelf)
        //     return;
        comboTxt.transform.DOBlendableLocalRotateBy(Vector3.up*360, 0.25f, RotateMode.FastBeyond360).OnComplete(() => TransformdateInitial());
    }
    private void TransformdateInitial()
    {
        comboTxt.transform.DOScale(comboTxtDefaultSize, 1).SetEase(Ease.OutBack);
        comboTxt.transform.DOMove(comboTxtDefaultPos, 1).SetEase(Ease.OutBack);
        // comboTxt.transform.localScale = comboTxtDefaultSize;
        // comboTxt.transform.position = comboTxtDefaultPos;
    }
}
 