﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    public  Transform parEffect;
    public GameEvent type;
    public float lifeTime;
    public float speed = 1;
    public Transform target;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Destroy(this.gameObject, lifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            rb.velocity = (target.position - transform.position).normalized * 10 * speed;
        }
        else
        {
            rb.velocity = new Vector3(-10 * speed, 0, 0);
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag != "Player")
            return;
        CreateEffect();
        DestroyEvent();
    }

    public void DestroyEvent()
    {
        CreateEffect();
        Destroy(this.gameObject);
    }
    private void CreateEffect()
    {
        if(parEffect == null)
            return;

        var par = Instantiate(parEffect, transform.position, Quaternion.identity);
        Destroy(par.gameObject, 0.5f);
    }
}
