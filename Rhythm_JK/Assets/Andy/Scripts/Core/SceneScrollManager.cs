﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneScrollManager : MonoBehaviour
{
    [SerializeField]
    public class ScrollData
    {
        public Vector3 createPoint;
        public Transform prospect;
        public float spdScale;
    }
    public ScrollData[] scrollDatas;
    public float counterCreateLimit;
    private float counter;

    // Update is called once per frame
    void Update()
    {
        
    }
}
