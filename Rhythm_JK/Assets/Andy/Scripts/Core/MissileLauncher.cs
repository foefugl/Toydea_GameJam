﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MissileLauncher : MonoBehaviour
{
    public GameEvent currentHitItem;
    public Transform playerTrans;
    public Transform[] attackTargets;
    public Transform missilePrefab;
    public int gizzCounterGap = 5;
    public int gizzAmout = 4;
    private bool bigGizz = false;
    private Missile currentMissile;

    // Start is called before the first frame update
    void Start()
    {
        ComboManager.Instance.Regist(ComBoEvent.ValueAdd, Attack);
        ComboManager.Instance.Regist(ComBoEvent.ValueAdd, BigGizz);
    }
    
    private void Attack()
    {
        var missile = Instantiate(missilePrefab, transform.position, Quaternion.Euler(transform.forward));
        currentMissile = missile.GetComponent<Missile>();

        if(currentHitItem == GameEvent.upperCollect)
        {
            currentMissile.target = attackTargets[0];
        }
        else if(currentHitItem == GameEvent.lowerCollect)
        {
            currentMissile.target = attackTargets[1];
        }
    }
    private void BigGizz()
    {
        if(ComboManager.Instance.ComboCount % gizzCounterGap != 0)
            return;
        if(FeverManager.Instance.feverHappen)
            return;

        StartCoroutine(MotionPresent());
    }
    private IEnumerator MotionPresent()
    {
        playerTrans.DOShakeScale(0.25f, 1, 10, 90, true).SetLoops(4).SetEase(Ease.OutExpo);
        yield return new WaitForSeconds(1);
        playerTrans.DOBlendableLocalRotateBy(Vector3.up * 360, 0.5f, RotateMode.FastBeyond360).SetEase(Ease.InCubic);
        bigGizz = true;
        for(int i=0 ; i<gizzAmout ; i++)
        {
            Attack();
            if(i == 0)
            {
                currentMissile.isSampleMissile = true;
            }
        }
        
    }
}
