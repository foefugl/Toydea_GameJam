﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Missile : MonoBehaviour 
{
	public Transform target;
	public Transform curvePoint;
	public GameObject explosion;
	public float outControlDis = 1.75f;
	public float lifeTime = 10;

	[System.Serializable]
	public class SpeedStruct
	{
		public float multiScale;
		public Vector2 speedLimit;
	}
	public SpeedStruct moveSpeedData;
	public SpeedStruct rotSpeedData;
	private float reactingSpeed;
	private float rotateSpeed;
	private Collider selfCollider;
	private bool freeChase;
	private bool isOutControl;
	private TimerCount lifeCounter;
	public bool isSampleMissile = false;

	// Use this for initialization
	void Start () 
	{
		reactingSpeed = moveSpeedData.speedLimit.x;
		rotateSpeed = rotSpeedData.speedLimit.x;
		curvePoint.parent = null;
		selfCollider  = GetComponent<Collider>();
	}
	private void OnEnable()
	{
		freeChase = true;
		isOutControl = false;
		lifeCounter = new TimerCount(lifeTime);
		StartCoroutine(FreeRotateSession());
	}
	
	// Update is called once per frame
	void Update () 
	{
		//TODO : 飛彈需要朝向玩家做攻擊，逐漸隨著時間反應速度越來越快，最後追擊目標物

		//Step1 : 抓到面向目標的Rotation，讓飛彈可以做出Rotation.Slerp等反應
		var rotTemp = transform.rotation;
		// var chaseTarget = (freeChase)? curvePoint : target ;

		if(!freeChase)curvePoint.position =  target.position;
		// if(!freeChase)curvePoint.position =  target.position;
		transform.LookAt(curvePoint);
		var finalRot = transform.rotation;
		transform.rotation = rotTemp;

		//如果飛彈和玩家距離大於追撞距離限制，則飛彈持續追蹤，反則直接追撞
		float dis = Vector3.Distance(transform.position, target.position);
		if(dis < outControlDis && isOutControl == false)
			isOutControl = true;
		if(isOutControl == false)
			transform.rotation = Quaternion.Slerp(transform.rotation, finalRot, Time.deltaTime*rotateSpeed);

		//Step2 : 讓飛彈做前進追撞
		transform.position += transform.forward * Time.deltaTime * reactingSpeed;

		//Step3 : 讓reactingSpeed逐漸加速，擬真飛彈的行為模式
		reactingSpeed = Mathf.Lerp(reactingSpeed, moveSpeedData.speedLimit.y, Time.deltaTime * moveSpeedData.multiScale);

		rotateSpeed = Mathf.Lerp(rotateSpeed, rotSpeedData.speedLimit.y, Time.deltaTime *rotSpeedData.multiScale);

		//萬一處理：如果飛彈沒有完成碰撞，避免他在遊戲持續存在，在LifeTime之後將他刪除
		lifeCounter.TimerUpdate();
		if(lifeCounter.IsFinish()){
			Destroy(gameObject);
			Destroy(curvePoint.gameObject);
		}
	}
	private IEnumerator FreeRotateSession()
	{
		freeChase = true;

		//TODO : 一開始隨機給予砲口一個遠得要命的方向（但要限定在一定範圍內），讓飛彈可以自由衝刺，過三秒後則讓他追逐原先目標
		Vector3 chaseOffset = new Vector3(0, Random.Range(4, 10.0f), Random.Range(-2.0f, 2.0f));
		var freeChasePos = transform.InverseTransformVector(chaseOffset) + transform.position;
		curvePoint.position = freeChasePos;
		target = curvePoint;

		yield return new WaitForSeconds(0.5f);
		Debug.Log("turn off : " + freeChase);
		freeChase = false;
	}
	private void OnTriggerStay(Collider other)
	{
		if(other.transform != target)
			return;
			
		//產生鏡頭震動、爆炸的特效
		// CameraShake.instance.ShakeIt();
		// var explore = Instantiate(explosion, transform.position, Quaternion.identity);

		//找出爆炸範圍內所有應該被影響的事物，並對其施力
		// Collider[] inExploreRangeColldier= Physics.OverlapSphere (transform.position, 10);
		// for(int i=0 ; i<inExploreRangeColldier.Length ; i++)
		// {
		// 	var inRangeRb = inExploreRangeColldier[i].GetComponent<Rigidbody>();
		// 	// if(inRangeRb != null)
		// 	// 	inRangeRb.AddExplosionForce(10, transform.position, 10, 6, ForceMode.Impulse);
		// }

		//將相關的Particle和自己的模型Destroy，包括幫助一開始的freePivot也要刪掉
		// ParticleSystem parts = explosion.GetComponent<ParticleSystem>();
		// float totalDuration = parts.duration;
		
		// if(other.transform.name != "slowMotion")Destroy(transform.gameObject);

		var hitGirl = other.transform.GetComponent<BulletItemHitGirl>();
		hitGirl.GirlPosHittedEff();
		BompInCalculateSystem.Instance.EventTrigger(hitGirl.eventType);
		if(isSampleMissile)GirlAniControl.Instance.GirlGetHit();
		if(other.isTrigger != true)
		{
			Destroy(transform.gameObject);
			// Destroy(explore.gameObject, totalDuration);
			// Destroy(curvePoint.gameObject, totalDuration);
		}
	}
	private void OnDrawGizmos()
	{
#if UNITY_EDITOR
		Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, outControlDis);
#endif
	}
}
