﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventObjHome : MonoBehaviour
{
    public float speed = 1;
    public enum CreateStatus
    {
        BompTime,
        RewardTime
    }
    public CreateStatus createStatus = CreateStatus.RewardTime;
    public AudioClip _tap;
    public AudioClip _click;
    public TempoSoundManager tempoSoundManager;
    public Transform[] createPointGroup;
    public Transform upperDestroy;
    public Transform lowerDestroy;
    public Transform obstacles;
    public Vector2 instanceDelayT;
    [Range(0,1)]
    public float scaleTemp;
    private GameEvent instantiateType = GameEvent.obstaclesCollect;
    private float createObjTimer = 0;
    private float instanceObjTLimit = 0;
    public int frameCountUpdate = 0;

    public void CreateNote(string noteCode)
    {
        if (this.enabled == false)
            return;


        if(createStatus == CreateStatus.BompTime)
        {
            if(noteCode == "w")
            {
                // tempoSoundManager.PlaySound(_click, 1);
                Instantiate(obstacles, createPointGroup[0].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;
            }
            else if(noteCode == "s")
            {   
                // tempoSoundManager.PlaySound(_click, 1);
                Instantiate(obstacles, createPointGroup[1].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;
            }
            else
            {
                var oneToCreateObstacle = Random.Range(0, 7);
                if(oneToCreateObstacle == 0)
                {
                    // tempoSoundManager.PlaySound(_click, 1);
                    Instantiate(upperDestroy, createPointGroup[0].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;
                }
                else if(oneToCreateObstacle == 1)
                {
                    // tempoSoundManager.PlaySound(_click, 1);
                    Instantiate(lowerDestroy, createPointGroup[1].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;  
                }
            }
        }
        else
        {
            if(noteCode == "w")
            {
                tempoSoundManager.PlaySound(_click, 1);
                Instantiate(upperDestroy, createPointGroup[0].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;
            }
            else if(noteCode == "s")
            {
                tempoSoundManager.PlaySound(_click, 1);
                Instantiate(lowerDestroy, createPointGroup[1].position, Quaternion.identity).GetComponent<ItemBehaviour>().speed = speed;
            }
        }
    }
}
