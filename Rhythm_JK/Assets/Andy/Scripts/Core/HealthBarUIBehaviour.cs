﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HealthBarUIBehaviour : MonoBehaviour
{
    public Image registHealthBar;
    public Text healthBarTxt;
    public GameEvent registEventType;
    public float changeValueSpd;
    private float bloodAmout = 1;
    private void Start()
    {
        Action hitEvent = HitEvent;
        BompInCalculateSystem.Instance.Regist(registEventType, hitEvent);
    }

    // Update is called once per frame
    private void Update()
    {
        registHealthBar.fillAmount = Mathf.Lerp(registHealthBar.fillAmount, bloodAmout, Time.deltaTime * changeValueSpd);
    }

    private void HitEvent()
    {
        if(FeverManager.Instance.feverHappen && registEventType == GameEvent.obstaclesCollect)
            return;
        
        if(bloodAmout > 0)
        bloodAmout -= 0.1f;
        healthBarTxt.text = (bloodAmout * 100).ToString();
    }
}
