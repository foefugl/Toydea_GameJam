﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlAniControl : MonoBehaviour
{
    public static GirlAniControl Instance;
    public Animator animator;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
    }

    public void GirlGetHit()
    {
        Debug.Log("Big Hit");
        animator.CrossFade("DragonGirl_Dam", 0.1f, 0);
    }
}
