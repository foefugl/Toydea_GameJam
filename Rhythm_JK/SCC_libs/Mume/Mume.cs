﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DressHP
{
    private float[] dress = new float[3];
    /* 0 is upper garment
    * 1 is middle garment
    * 2 is lower garment
    */
    public int getDressLength()
    {
        return dress.Length;
    }
    public float getHP(int _whitchDress)
    {
        return dress[_whitchDress];
    }
    public void setHP(int _whitchDress, float _hp)
    {
        dress[_whitchDress] = _hp;
    }
    public void reduceHP(int _whitchDress, float value)
    {
        if (dress[_whitchDress] - value > 0f)
            dress[_whitchDress] -= value;
        else
            dress[_whitchDress] = 0f;
    }
    public float getTotalDressHP()
    {
        float sum = 0f;
        for (int i = 0; i < dress.Length; i++)
            sum += dress[i];
        return sum;
    }
    public void init()
    {
        for (int i = 0; i < dress.Length; i++)
        {
            setHP(i, 100f);
        }
    }
}
public class Mume : MonoBehaviour
{
    // Start is called before the first frame update
    public DressHP dressHP;

    public float wantDestroyParticularDress(int _whitchDress)
    {
        float totalDressHP = dressHP.getTotalDressHP();
        float totalExp = 0f;
        for (int i = 0; i < dressHP.getDressLength(); i++)
            totalExp += Mathf.Exp(2 - (dressHP.getHP(i) / totalDressHP));
        float wantExp = Mathf.Exp(2 - (dressHP.getHP(_whitchDress) / totalDressHP));
        return wantExp / totalExp;
    }
    private bool canDestroyDress(int _whitchDress)
    {
        return dressHP.getHP(_whitchDress) <= 0;
    }
    public void upperDressDestroy()
    {
        if (canDestroyDress(0))
        {

        }
    }
    public void middleDressDestroy()
    {
        if (canDestroyDress(1))
        {

        }
    }
    public void lowerDressDestroy()
    {
        if (canDestroyDress(2))
        {

        }
    }
    public void reduceDressHP(int _whitchDress, float value)
    {
        dressHP.reduceHP(_whitchDress, value);
    }
    void Start()
    {
        dressHP.init();
        reduceDressHP(0, 10);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(wantDestroyParticularDress(0));
    }
}