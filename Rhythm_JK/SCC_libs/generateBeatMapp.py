#from shutil import copyfile
import sys
#import zipfile

isStartHitObjects = False

if len(sys.argv) < 2:
    print("no input file")
    sys.exit()

# with zipfile.ZipFile(sys.argv[1], 'r') as zip_ref:
#    zip_ref.extractall("")

def isHitObjects(line):
    global isStartHitObjects
    if (isStartHitObjects):
        return True
    elif (line == '[HitObjects]'):
        isStartHitObjects = True
        return False
    else:
        return False


f = open(sys.argv[1]).read().split('\r')
outFile = open("/mnt/c/Users/yang/git/SummerTime_GameJam/Rhythm_JK/SCC_libs/"+"beatMap.txt", 'w+')
for i in f:
    for j in i.split():
        print(j)
        if(isHitObjects(j)):
            arr = j.split(',')
            outFile.write(arr[2] + '\n')

#ref: https: // osu.ppy.sh/help/wiki/osu!_File_Formats/Osu_(file_format)
#song: https://osu.ppy.sh/beatmapsets/956590#osu/2002605
